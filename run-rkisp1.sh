#!/bin/bash

aarch64-linux-gnu-gcc -g rkisp1.c  -o rkisp1-arm64
arm-linux-gnueabihf-gcc -g rkisp1.c  -o rkisp1-arm

pahole rkisp1-arm > rkisp1-arm-pahole
pahole rkisp1-arm64 > rkisp1-arm64-pahole

PHASH=$(sha1sum rkisp1-arm-pahole | cut -d ' ' -f 1)
PHASH64=$(sha1sum rkisp1-arm64-pahole | cut -d ' ' -f 1)

echo -e "arm   pahole sha: $PHASH"
echo -e "arm64 pahole sha: $PHASH64"

