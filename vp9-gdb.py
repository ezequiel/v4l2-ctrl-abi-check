#!/usr/bin/env python3

import gdb.types

def print_struct(name):
    struct = gdb.lookup_type(name)
    print(name)
    print("size %d" % struct.sizeof)
    print("\t\t\t\t\t\toffset\tsize")

    for k, v in gdb.types.deep_items(struct):
        field = struct[k]

        print("\t%-40s%d\t%d" % (field.name,
                                 field.bitpos / 8,
                                 field.type.sizeof))

    print()

print_struct("struct v4l2_ctrl_vp9_frame")
print_struct("struct v4l2_ctrl_vp9_compressed_hdr_probs")

gdb.execute("quit")
